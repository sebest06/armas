/*
 * AccesorioWeapon.h
 *
 *  Created on: 22/9/2018
 *      Author: sebest
 */

#ifndef ACCESORIOWEAPON_H_
#define ACCESORIOWEAPON_H_

#include <string>
#include "WeaponsAttributes.h"
#include "Item.h"

class AccesorioWeapon : public Item {
public:
	AccesorioWeapon(WeaponsAttributes *_attribute);
	virtual ~AccesorioWeapon();
	WeaponsAttributes* getAttributes();
	TipoAccesorio getTipoItem();

protected:
	WeaponsAttributes *atributos;
};

#endif /* ACCESORIOWEAPON_H_ */
