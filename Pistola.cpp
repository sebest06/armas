/*
 * Pistola.cpp
 *
 *  Created on: 22/9/2018
 *      Author: sebest
 */

#include "Pistola.h"
#include "WeaponsAttributes.h"
#include "Weapon.h"
#include "WeaponsAtributesValues.h"
#include <iostream>

Pistola::Pistola() : Weapon(new WeaponsAttributes(PISTOLA_ATRIBUTES)) {
	// TODO Auto-generated constructor stub
	cantidadAccesorios = 3;
}

Pistola::~Pistola() {
	// TODO Auto-generated destructor stub
}

bool Pistola::checkearAgregarAccesorio(AccesorioWeapon* _accesorio) {

	if(cantidadAccesorios > 0)
	{
		TipoAccesorio tipo = _accesorio->getAttributes()->getTipoAccesorio();

		if(tipo == ACCESORIO_FUEGO_CORTO)
		{
			return Weapon::checkearAgregarAccesorio(_accesorio);
		}
		return false;
	}
	return false;
}
