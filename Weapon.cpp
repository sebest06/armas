/*
 * Weapon.cpp
 *
 *  Created on: 22/9/2018
 *      Author: sebest
 */

#include "Weapon.h"
#include <iostream>
#include <cstdlib>
#include <iostream>

Weapon::Weapon(WeaponsAttributes *values) {
	// TODO Auto-generated constructor stub
	atributosDefault = values;
	atributosFinal = new WeaponsAttributes(values);
	municion = 0;
	level = 1;
	double tipo_rareza = std::rand()/(RAND_MAX + 1.0);
	if(tipo_rareza > 0.96)
	{
		rareza = 0.4;
	}
	else if(tipo_rareza > 0.78)
	{
		rareza = 0.15;
	}
	else
	{
		rareza = 0;
	}

    //std::cout << "Rareza = " << rareza  << " Value = " << tipo_rareza << std::endl;
	calcular();
}

Weapon::~Weapon() {
	// TODO Auto-generated destructor stub
}

bool Weapon::fire() {
	if(municion > 0)
	{
		municion--;
		return true;
	}
	return false;
}

void Weapon::calcular() {
	WeaponsAttributes *accesorio;
	atributosFinal->resetAttributes();
	atributosFinal->setBaseAttribute(atributosDefault);

	for (std::vector<AccesorioWeapon*>::iterator it = listAccesorios.begin() ; it != listAccesorios.end(); ++it)
	{
		accesorio = (*it)->getAttributes();
		atributosFinal->addAttributes(accesorio);
	}
	//TODO: Calcular parametros con rareza y nivel
}

bool Weapon::recargarMuniciones(Bullet* _municion) {

	if(_municion->getAttributes()->getTipoMunicion() == atributosDefault->getTipoMunicion())
	{
		_municion->sumarBalas(municion);
		municion = _municion->descontarBalas(atributosFinal->getCargador());
		return true;
	}
	return false;
}

bool Weapon::addAccesorio(AccesorioWeapon* accesorio) {
	if(checkearAgregarAccesorio(accesorio))
	{
		listAccesorios.push_back(accesorio);
		calcular();
		return true;
	}
	return false;
}

int Weapon::getMunicion() {
	return municion;
}

int Weapon::getCargador() {
	return atributosFinal->getCargador();
}

double Weapon::getPresicion() {
	return atributosFinal->getPresicion();
}

double Weapon::getAlcance() {
	return atributosFinal->getAlcance();
}

double Weapon::getDanio() {
	return atributosFinal->getDanio();
}

std::string Weapon::getName() {
	return atributosFinal->getNombre();
}

bool Weapon::levelUp() {
	if(level < MAX_WEAPON_LEVEL)
	{
		level++;
		return true;
	}
	return false;
}

int Weapon::getMaxLevel() {
	return MAX_WEAPON_LEVEL;
}

bool Weapon::checkearAgregarAccesorio(AccesorioWeapon* _accesorio) {
	if(cantidadAccesorios > 0)
	{
		for (std::vector<AccesorioWeapon*>::iterator it = listAccesorios.begin() ; it != listAccesorios.end(); ++it)
		{
			if(_accesorio->getAttributes()->getId() == (*it)->getAttributes()->getId())
			{
				return false;
			}
		}
		cantidadAccesorios--;
		return true;
	}
	return false;
}

bool Weapon::removeAccesorio(AccesorioWeapon* _accesorio) {
	AccesorioWeapon *accesorio;
	for (std::vector<AccesorioWeapon*>::iterator it = listAccesorios.begin() ; it != listAccesorios.end(); ++it)
	{
		accesorio = (*it);
		if(accesorio == _accesorio)
		{
			listAccesorios.erase(it);
			calcular();
			return 1;
		}
	}
	return 0;
}

TipoAccesorio Weapon::getTipoItem() {
	return atributosDefault->getTipoAccesorio();
}
