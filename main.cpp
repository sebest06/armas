#include <iostream>
#include <cstdlib>
#include <iostream>
#include <ctime>

#include "Game.h"
//#include "windows.h"
#include <iostream>
#include <SDL.h>

const int FPS = 60;
const int DELAY_TIME = 1000.0f / FPS;

int main(int argc, char **argv)
{
//	AllocConsole();
//	freopen("CON", "w", stdout);
    Uint32 frameStart, frameTime;

    std::cout << "game init attempt...\n";
    if(TheGame::Instance()->init("Nombre", 100, 100, 640, 480, false))
    {
        std::cout << "game init success!\n";
        while(TheGame::Instance()->running())
        {
            frameStart = SDL_GetTicks();

            TheGame::Instance()->handleEvents();
            TheGame::Instance()->update();
            TheGame::Instance()->render();

            frameTime = SDL_GetTicks() - frameStart;

            if(frameTime < DELAY_TIME)
            {
                SDL_Delay((int)(DELAY_TIME - frameTime));
            }
        }
    }
    else
    {
        std::cout << "game init failure - " << SDL_GetError() << "\n";
        return -1;
    }

    std::cout << "game closing...\n";
    TheGame::Instance()->clean();

    return 0;
}

/*#include "Weapon.h"
#include "Pistola.h"
#include "MiraTelescopica.h"
#include "Bullet.h"
#include "Item.h"
#include "Inventory.h"


int main()
{
	Inventory *inventario = new Inventory();
	std::srand(std::time(0));
	Weapon *miArma;
	miArma = new Pistola();
	miArma->recargarMuniciones(new Bullet(BALA_9mm,10));
	AccesorioWeapon *acc1 = new MiraTelescopica();
	AccesorioWeapon *acc2 = new MiraTelescopica();

	inventario->addToInventory(miArma);
	inventario->addToInventory(acc1);
	inventario->addToInventory(acc2);
	inventario->addToInventory(new Bullet(BALA_9mm,10));

	std::cout << "Nombre: " << miArma->getName() << std::endl;
	std::cout << "	Alcance:" << miArma->getAlcance() << std::endl;
	std::cout << "	Cargador:" << miArma->getCargador() << std::endl;
	std::cout << "	Municion:" << miArma->getMunicion() << std::endl;
	std::cout << "	Precision:" << miArma->getPresicion() << std::endl;
	std::cout << "	Danio:" << miArma->getDanio() << std::endl;

	miArma->fire();

	std::cout << "Nombre: " << miArma->getName() << std::endl;
	std::cout << "	Alcance:" << miArma->getAlcance() << std::endl;
	std::cout << "	Cargador:" << miArma->getCargador() << std::endl;
	std::cout << "	Municion:" << miArma->getMunicion() << std::endl;
	std::cout << "	Precision:" << miArma->getPresicion() << std::endl;
	std::cout << "	Danio:" << miArma->getDanio() << std::endl;

	miArma->addAccesorio(acc1);

	std::cout << "Nombre: " << miArma->getName() << std::endl;
	std::cout << "	Alcance:" << miArma->getAlcance() << std::endl;
	std::cout << "	Cargador:" << miArma->getCargador() << std::endl;
	std::cout << "	Municion:" << miArma->getMunicion() << std::endl;
	std::cout << "	Precision:" << miArma->getPresicion() << std::endl;
	std::cout << "	Danio:" << miArma->getDanio() << std::endl;

	miArma->addAccesorio(acc2);

	std::cout << "Nombre: " << miArma->getName() << std::endl;
	std::cout << "	Alcance:" << miArma->getAlcance() << std::endl;
	std::cout << "	Cargador:" << miArma->getCargador() << std::endl;
	std::cout << "	Municion:" << miArma->getMunicion() << std::endl;
	std::cout << "	Precision:" << miArma->getPresicion() << std::endl;
	std::cout << "	Danio:" << miArma->getDanio() << std::endl;

	miArma->removeAccesorio(acc2);

	std::cout << "Nombre: " << miArma->getName() << std::endl;
	std::cout << "	Alcance:" << miArma->getAlcance() << std::endl;
	std::cout << "	Cargador:" << miArma->getCargador() << std::endl;
	std::cout << "	Municion:" << miArma->getMunicion() << std::endl;
	std::cout << "	Precision:" << miArma->getPresicion() << std::endl;
	std::cout << "	Danio:" << miArma->getDanio() << std::endl;


	return 0;
}
*/
