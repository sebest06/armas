/*
 * WeaponsAttributes.h
 *
 *  Created on: 22/9/2018
 *      Author: sebest
 */

#ifndef WEAPONSATTRIBUTES_H_
#define WEAPONSATTRIBUTES_H_
#include <string>
#include "WeaponsAtributesValues.h"

class WeaponsAttributes {
public:
	WeaponsAttributes(WeaponsAttributes *_attribute);
	WeaponsAttributes(unsigned int _id,std::string _nombre, int _cargador,double _danio,double _presicion,double _alcance, TipoMunicion _tipo, TipoAccesorio _tipoAccesorio);
	virtual ~WeaponsAttributes();
	int getCargador();
	double getPresicion();
	double getAlcance();
	double getDanio();
	unsigned int getId();
	TipoMunicion getTipoMunicion();
	std::string getNombre();
	TipoAccesorio getTipoAccesorio(void);

	void setAttributes(WeaponsAttributes value);
	void setCargador(int _cargador);
	void setPresicion(double _presicion);
	void setAlcance(double _alcance);
	void setDanio(double _danio);
	void setTipoMunicion(TipoMunicion _tipo);
	void setNombre(std::string _nombre);
	void setTipoAccesorio(TipoAccesorio _tipoAccesorio);

	void resetAttributes();
	void setBaseAttribute(WeaponsAttributes *value);

	void addCargador(int _cargador);
	void addPresicion(double _presicion);
	void addAlcance(double _alcance);
	void addDanio(double _danio);
	void addAttributes(WeaponsAttributes *value);

protected:
	unsigned int id;
	int cargador;
	double danio;
	double presicion;
	double alcance;
	enum TipoMunicion tipo;
	std::string nombre;
	TipoAccesorio tipoAccesorio;

};

#endif /* WEAPONSATTRIBUTES_H_ */
