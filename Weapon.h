/*
 * Weapon.h
 *
 *  Created on: 22/9/2018
 *      Author: sebest
 */

#ifndef WEAPON_H_
#define WEAPON_H_

#include <vector>
#include <string>
#include "WeaponsAttributes.h"
#include "AccesorioWeapon.h"
#include "Item.h"
#include "Bullet.h"

#define MAX_WEAPON_LEVEL 50

class Weapon : public Item {
public:
	Weapon(WeaponsAttributes *values);
	virtual ~Weapon();
	virtual bool fire();
	bool addAccesorio(AccesorioWeapon *accesorio);
	bool removeAccesorio(AccesorioWeapon *accesorio);
	void calcular();
	bool recargarMuniciones(Bullet* _municion);
	int getMunicion();
	int getCargador();
	double getPresicion();
	double getAlcance();
	double getDanio();
	std::string getName();
	bool levelUp();
	virtual int getMaxLevel();
	TipoAccesorio getTipoItem();

private:

protected:
	//TODO: COSTO DE COMPRA, COSTO DE VENTA, DURABILIDAD

	virtual bool checkearAgregarAccesorio(AccesorioWeapon *_accesorio);
	int municion;
	int level;
	double rareza;
	WeaponsAttributes *atributosDefault;
	WeaponsAttributes *atributosFinal;
	std::vector<AccesorioWeapon*> listAccesorios;
	int cantidadAccesorios;
};

#endif /* WEAPON_H_ */
