/*
 * Item.h
 *
 *  Created on: 28/9/2018
 *      Author: electro1
 */

#ifndef ITEM_H_
#define ITEM_H_

#include "WeaponsAtributesValues.h"

class Item {
public:
	Item();
	virtual ~Item();
	virtual TipoAccesorio getTipoItem() = 0;

};

#endif /* ITEM_H_ */
