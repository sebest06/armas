/*
 * MiraTelescopica.h
 *
 *  Created on: 22/9/2018
 *      Author: sebest
 */

#ifndef MIRATELESCOPICA_H_
#define MIRATELESCOPICA_H_

#include "AccesorioWeapon.h"

class MiraTelescopica : public AccesorioWeapon{
public:
	MiraTelescopica();
	virtual ~MiraTelescopica();
};

#endif /* MIRATELESCOPICA_H_ */
