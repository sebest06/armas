/*
 * Bullet.cpp
 *
 *  Created on: 28/9/2018
 *      Author: electro1
 */

#include "Bullet.h"

Bullet::Bullet(TipoMunicion Tipo, int cantidadBalas) : AccesorioWeapon(new WeaponsAttributes(BALAS_ATRIBUTOS)) {
	// TODO Auto-generated constructor stub
	cantidadDeBalas = cantidadBalas;
	atributos->setTipoMunicion(Tipo);
}

Bullet::~Bullet() {
	// TODO Auto-generated destructor stub
}

bool Bullet::restarBalas(int cantidad) {
	if(cantidad <= cantidadDeBalas)
	{
		cantidadDeBalas -= cantidad;
		return true;
	}
	return false;
}

int Bullet::getBalas() {
	return cantidadDeBalas;
}

bool Bullet::vacio() {
	return cantidadDeBalas == 0 ? true : false;
}

int Bullet::descontarBalas(int sizeCargador) {
	int desconto = 0;
	if(cantidadDeBalas >= sizeCargador)
	{
		cantidadDeBalas -= sizeCargador;
		desconto = sizeCargador;
	}
	else
	{
		desconto = cantidadDeBalas;
		cantidadDeBalas = 0;
	}
	return desconto;
}

bool Bullet::sumarBalas(int cantidad) {
	cantidadDeBalas += cantidad;
	return true;
}
