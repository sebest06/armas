/*
 * WeaponsAttributes.cpp
 *
 *  Created on: 22/9/2018
 *      Author: sebest
 */

#include "WeaponsAttributes.h"

WeaponsAttributes::WeaponsAttributes(WeaponsAttributes *_attribute) {
	// TODO Auto-generated constructor stub
	cargador = _attribute->getCargador();
	danio = _attribute->getDanio();
	presicion = _attribute->getPresicion();
	alcance = _attribute->getAlcance();
	tipo = _attribute->getTipoMunicion();
	tipoAccesorio = _attribute->getTipoAccesorio();
	id = _attribute->getId();
}

WeaponsAttributes::~WeaponsAttributes() {
	// TODO Auto-generated destructor stub
}

int WeaponsAttributes::getCargador() {
	return cargador;
}

double WeaponsAttributes::getPresicion() {
	return presicion;
}

double WeaponsAttributes::getAlcance() {
	return alcance;
}

double WeaponsAttributes::getDanio() {
	return danio;
}

TipoMunicion WeaponsAttributes::getTipoMunicion() {
	return tipo;
}

unsigned int WeaponsAttributes::getId()
{
	return id;
}

void WeaponsAttributes::setAttributes(WeaponsAttributes value) {
	cargador = value.getCargador();
	danio = value.getDanio();
	presicion = value.getPresicion();
	alcance = value.getAlcance();
	tipo = value.getTipoMunicion();
	nombre = value.nombre;
}

void WeaponsAttributes::setCargador(int _cargador) {
	cargador = _cargador;
}

void WeaponsAttributes::setPresicion(double _presicion) {
	presicion = _presicion;
}

void WeaponsAttributes::setAlcance(double _alcance) {
	alcance = _alcance;
}

void WeaponsAttributes::setDanio(double _danio) {
	danio = _danio;
}

void WeaponsAttributes::setTipoMunicion(TipoMunicion _tipo) {
	tipo = _tipo;
}

WeaponsAttributes::WeaponsAttributes(unsigned int _id,std::string _nombre,int _cargador, double _danio,
		double _presicion, double _alcance, TipoMunicion _tipo, TipoAccesorio _tipoAccesorio) {
	id = _id;
	cargador = _cargador;
	danio = _danio;
	presicion = _presicion;
	alcance = _alcance;
	tipo = _tipo;
	nombre = _nombre;
	tipoAccesorio = _tipoAccesorio;
}

void WeaponsAttributes::addCargador(int _cargador) {
	cargador += _cargador;
}

void WeaponsAttributes::addPresicion(double _presicion) {
	presicion *= (1+_presicion);
}

void WeaponsAttributes::addAlcance(double _alcance) {
	alcance *= (1+_alcance);
}

void WeaponsAttributes::addDanio(double _danio) {
	danio *= (1+_danio);
}

std::string WeaponsAttributes::getNombre() {
	return nombre;
}

TipoAccesorio WeaponsAttributes::getTipoAccesorio(void) {
	return tipoAccesorio;
}

void WeaponsAttributes::setTipoAccesorio(TipoAccesorio _tipoAccesorio) {
	tipoAccesorio = _tipoAccesorio;
}

void WeaponsAttributes::addAttributes(WeaponsAttributes *value) {
	if(tipo == value->getTipoMunicion())
	{
		cargador += value->getCargador();
		presicion *= (1+value->getPresicion());
		alcance *= (1+value->getAlcance());
		danio *= (1+value->getDanio());
		nombre += " " + value->getNombre();
	}
}

void WeaponsAttributes::setNombre(std::string _nombre) {
	nombre = _nombre;
}

void WeaponsAttributes::resetAttributes() {
	cargador = 0;
	danio = 1;
	presicion = 1;
	alcance = 1;
}

void WeaponsAttributes::setBaseAttribute(WeaponsAttributes* value) {
	cargador = value->getCargador();
	danio = value->getDanio();
	presicion = value->getPresicion();
	alcance = value->getAlcance();
	tipo = value->getTipoMunicion();
	nombre = value->nombre;
}
