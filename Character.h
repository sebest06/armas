/*
 * Character.h
 *
 *  Created on: 29/9/2018
 *      Author: sebest
 */

#ifndef CHARACTER_H_
#define CHARACTER_H_
#include "Weapon.h"

class Character {
public:
	Character();
	virtual ~Character();
	double fire(){};
	bool setWeapon(Weapon *unArma){};
	bool setCargador(Bullet *bullets){};
protected:
	int life;
	Weapon *arma;


};

#endif /* CHARACTER_H_ */
