/*
 * Inventory.h
 *
 *  Created on: 28/9/2018
 *      Author: electro1
 */

#ifndef INVENTORY_H_
#define INVENTORY_H_
#include <map>
#include "Item.h"

class Inventory {
public:
	Inventory();
	virtual ~Inventory();
	bool addToInventory(Item *unItem);
	Item* removeToInventory(int posicion);
	Item* getItem(int posicion);

private:
	Item** misItems;
};

#endif /* INVENTORY_H_ */
