/*
 * Inventory.cpp
 *
 *  Created on: 28/9/2018
 *      Author: electro1
 */

#include "Inventory.h"
#define SIZEOF_INVENTORY 50

Inventory::Inventory() {
	// TODO Auto-generated constructor stub
	misItems = new Item*[SIZEOF_INVENTORY];
	for(unsigned int i = 0; i < SIZEOF_INVENTORY; i++)
	{
		misItems[i] = 0;
	}
}

Inventory::~Inventory() {
	// TODO Auto-generated destructor stub
}

bool Inventory::addToInventory(Item* unItem) {
	bool salida = 0;
	for(unsigned int i = 0; i < SIZEOF_INVENTORY; i++)
	{
		if(misItems[i] == 0)
		{
			misItems[i] = unItem;
			salida = 1;
		}
	}
	return salida;
}

Item* Inventory::removeToInventory(int posicion) {
	Item* itemRemoved = misItems[posicion];
	misItems[posicion] = 0;
	return itemRemoved;
}

Item* Inventory::getItem(int posicion) {
	return misItems[posicion];
}
