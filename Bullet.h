/*
 * Bullet.h
 *
 *  Created on: 28/9/2018
 *      Author: electro1
 */

#ifndef BULLET_H_
#define BULLET_H_

#include "AccesorioWeapon.h"

class Bullet : public AccesorioWeapon{
public:
	Bullet(TipoMunicion Tipo, int cantidadBalas);
	virtual ~Bullet();
	bool restarBalas(int cantidad);
	int getBalas();
	bool vacio();
	int descontarBalas(int sizeCargador);
	bool sumarBalas(int cantidad);

private:
	int cantidadDeBalas;
};

#endif /* BULLET_H_ */
