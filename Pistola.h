/*
 * Pistola.h
 *
 *  Created on: 22/9/2018
 *      Author: sebest
 */

#ifndef PISTOLA_H_
#define PISTOLA_H_

#include <vector>
#include "Weapon.h"
#include "WeaponsAtributesValues.h"

class Pistola : public Weapon{
public:
	Pistola();
	virtual ~Pistola();

protected:
	bool checkearAgregarAccesorio(AccesorioWeapon *_accesorio);

private:


};

#endif /* PISTOLA_H_ */
