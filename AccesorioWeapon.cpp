/*
 * AccesorioWeapon.cpp
 *
 *  Created on: 22/9/2018
 *      Author: sebest
 */

#include "AccesorioWeapon.h"

AccesorioWeapon::~AccesorioWeapon() {
	// TODO Auto-generated destructor stub
}

AccesorioWeapon::AccesorioWeapon(WeaponsAttributes* _attribute) {
	atributos = _attribute;
}

WeaponsAttributes* AccesorioWeapon::getAttributes() {
	return atributos;
}

TipoAccesorio AccesorioWeapon::getTipoItem() {
	return atributos->getTipoAccesorio();
}
